# Czech PO debconf template translation of glance.
# Copyright (C) 2012 Michal Simunek <michal.simunek@gmail.com>
# This file is distributed under the same license as the glance package.
# Michal Simunek <michal.simunek@gmail.com>, 2012 - 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: glance 2013.1.2-4\n"
"Report-Msgid-Bugs-To: openstack-pkg-tools@packages.debian.org\n"
"POT-Creation-Date: 2019-10-10 18:44+0200\n"
"PO-Revision-Date: 2013-08-25 13:01+0200\n"
"Last-Translator: Michal Simunek <michal.simunek@gmail.com>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-db.templates:1001
#, fuzzy
#| msgid "Set up a database for glance?"
msgid "Set up a database for this package?"
msgstr "Nastavit databázi pro glance?"

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-db.templates:1001
#, fuzzy
#| msgid ""
#| "No database has been set up for glance-registry or glance-api to use. "
#| "Before continuing, you should make sure you have:"
msgid ""
"No database has been set up for this package. Before continuing, you should "
"make sure you have the following information:"
msgstr ""
"glance-registry, nebo glance-api, nemá nastavenu žádnou databázi k "
"používání. Před tím, než budete pokračovat se ujistěte že máte:"

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-db.templates:1001
#, fuzzy
#| msgid ""
#| " - the server host name (that server must allow TCP connections from "
#| "this\n"
#| "   machine);\n"
#| " - a username and password to access the database.\n"
#| " - A database type that you want to use."
msgid ""
" * the type of database that you want to use - generally the MySQL backend\n"
"   (which is compatible with MariaDB) is a good choice, and other\n"
"   implementations like PostgreSQL or SQLite are often problematic with\n"
"   OpenStack (this depends on the service);\n"
" * the database server hostname (that server must allow TCP connections "
"from\n"
"   this machine);\n"
" * a username and password to access the database."
msgstr ""
" - název hostitelského serveru (tento server musí přijímat TCP spojení\n"
"   z tohoto počítače);\n"
" - uživatelské jméno a heslo pro přístup k databázi.\n"
" - Typ databáze, kterou chcete používat."

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-db.templates:1001
msgid ""
"Note that if you plan on using a remote database server, you must first "
"configure dbconfig-common to do so (using dpkg-reconfigure dbconfig-common), "
"and the remote database server needs to be configured with adequate "
"credentials."
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-db.templates:1001
#, fuzzy
#| msgid ""
#| "If some of these requirements are missing, reject this option and run "
#| "with regular sqlite support."
msgid ""
"If some of these requirements are missing, do not choose this option. Run "
"with regular SQLite support instead."
msgstr ""
"Pokud některou z těchto povinných voleb neznáte, přeskočte ji a glance "
"spouštějte s běžnou podporou sqlite."

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-db.templates:1001
#, fuzzy
#| msgid ""
#| "You can change this setting later on by running 'dpkg-reconfigure -plow "
#| "glance-common'."
msgid ""
"You can change this setting later on by running \"dpkg-reconfigure -plow\"."
msgstr ""
"Toto nastavení můžete později změnit spuštěním 'dpkg-reconfigure -plow "
"glance-common'."

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:1001
msgid "Manage keystone_authtoken with debconf?"
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:1001
msgid ""
"Every OpenStack service must contact Keystone, and this is configured "
"through the [keystone_authtoken] section of the configuration. Specify if "
"you wish to handle this configuration through debconf."
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:2001
#, fuzzy
#| msgid "Auth server username:"
msgid "Auth server public endpoint URL:"
msgstr "Uživatel autentizačního serveru:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:2001
msgid ""
"Specify the URL of your Keystone authentication server public endpoint. This "
"value will be set in the www_authenticate_uri directive."
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:3001
#, fuzzy
#| msgid "Keystone Auth Token:"
msgid "Keystone region:"
msgstr "Autentizační klíč pro Keystone:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:3001
msgid "Specify the Keystone region to use."
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:3001
#: ../openstack-pkg-tools.configure-endpoint.templates:9001
msgid ""
"The region name is usually a string containing only ASCII alphanumerics, "
"dots, and dashes."
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:4001
msgid "Create service user?"
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:4001
msgid ""
"This package can reuse an already existing username, or create one right "
"now. If you wish to create one, then you will be prompted for the admin "
"credentials."
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:5001
#, fuzzy
#| msgid "Auth server username:"
msgid "Auth server admin username:"
msgstr "Uživatel autentizačního serveru:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:6001
#, fuzzy
#| msgid "Auth server tenant name:"
msgid "Auth server admin project name:"
msgstr "Název nájemce pro autentizační server:"

#. Type: password
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:7001
#, fuzzy
#| msgid "Auth server password:"
msgid "Auth server admin password:"
msgstr "Heslo autentizačního serveru:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:8001
#, fuzzy
#| msgid "Auth server username:"
msgid "Auth server service username:"
msgstr "Uživatel autentizačního serveru:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:9001
#, fuzzy
#| msgid "Auth server username:"
msgid "Auth server service project name:"
msgstr "Uživatel autentizačního serveru:"

#. Type: password
#. Description
#: ../openstack-pkg-tools.configure-ksat.templates:10001
#, fuzzy
#| msgid "Auth server password:"
msgid "Auth server service password:"
msgstr "Heslo autentizačního serveru:"

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:1001
msgid "Configure RabbitMQ access with debconf?"
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:1001
msgid ""
"OpenStack services need access to a message queue server, defined by the "
"transport_url directive. Please specify whether configuring this should be "
"handled through debconf."
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:1001
msgid ""
"Only access to RabbitMQ is handled, and the RabbitMQ user creation isn't "
"performed. A new RabbitMQ user can be created with the commands:\""
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:1001
msgid ""
" - rabbitmqctl add_user openstack PASSWORD\n"
" - rabbitmqctl set_permissions -p / openstack \".*\" \".*\" \".*\""
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:1001
msgid ""
"Note that the default RabbitMQ guest account cannot be used for remote "
"connections."
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:2001
msgid "IP address of your RabbitMQ host:"
msgstr ""

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: password
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:2001
#: ../openstack-pkg-tools.configure-rabbit.templates:3001
#: ../openstack-pkg-tools.configure-rabbit.templates:4001
msgid ""
"In order to interoperate with other components of OpenStack, this package "
"needs to connect to a central RabbitMQ server."
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:2001
msgid "Please specify the IP address of that server."
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:3001
msgid "Username for connection to the RabbitMQ server:"
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:3001
msgid "Please specify the username used to connect to that RabbitMQ server."
msgstr ""

#. Type: password
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:4001
msgid "Password for connection to the RabbitMQ server:"
msgstr ""

#. Type: password
#. Description
#: ../openstack-pkg-tools.configure-rabbit.templates:4001
msgid "Please specify the password used to connect to that RabbitMQ server."
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:1001
#, fuzzy
#| msgid "Register Glance in the keystone endpoint catalog?"
msgid "Register this service in the Keystone endpoint catalog?"
msgstr "Zaregistrovat Glance v katalogu koncových bodů keystone?"

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:1001
#, fuzzy
#| msgid ""
#| "Each Openstack services (each API) should be registered in order to be "
#| "accessible. This is done using \"keystone service-create\" and \"keystone "
#| "endpoint-create\". Select if you want to run these commands now."
msgid ""
"Each OpenStack service (each API) must be registered in the Keystone catalog "
"in order to be accessible. This is done using \"openstack service create\" "
"and \"openstack endpoint create\". This can be done automatically now."
msgstr ""
"Aby byla každá služba Openstack (každé API) přístupná, musí být "
"zaregistrována. To se provádí pomocí příkazů \"keystone service-create\" a "
"\"keystone endpoint-create\". Zvolte si, zda-li se tyto příkazy mají nyní "
"spustit."

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:1001
#, fuzzy
#| msgid ""
#| "Note that you will need to have an up and running keystone server on "
#| "which to connect using the Keystone auth token."
msgid ""
"Note that you will need to have an up and running Keystone server on which "
"to connect using a known admin project name, admin username and password. "
"The admin auth token is not used anymore."
msgstr ""
"Berte na vědomí, že musíte mít běžící server keystone, na který se lze "
"připojit pomocí ověřovacího klíče pro Keystone."

#. Type: boolean
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:1001
msgid ""
"Also, if a service with a matching name is already present in the Keystone "
"catalog, endpoint registration will be aborted."
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:2001
#, fuzzy
#| msgid "Keystone IP address:"
msgid "Keystone server address:"
msgstr "IP adresa serveru keystone:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:2001
#, fuzzy
#| msgid ""
#| "Enter the IP address of your keystone server, so that glance-api can "
#| "contact Keystone to do the Glance service and endpoint creation."
msgid ""
"Please enter the address (IP or resolvable address) of the Keystone server, "
"for creating the new service and endpoints."
msgstr ""
"Zadejte IP adresu serveru keystone, aby se mohlo glance-api spojit s "
"Keystone a provozovat službu Glance a vytvářet koncové body."

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:2001
msgid ""
"Any non-valid ipv4, ipv6 or host address string will abort the endpoint "
"registration."
msgstr ""

#. Type: select
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:3001
#, fuzzy
#| msgid "Keystone Auth Token:"
msgid "Keystone endpoint protocol:"
msgstr "Autentizační klíč pro Keystone:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:4001
#, fuzzy
#| msgid "Keystone Auth Token:"
msgid "Keystone admin username:"
msgstr "Autentizační klíč pro Keystone:"

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: password
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:4001
#: ../openstack-pkg-tools.configure-endpoint.templates:5001
#: ../openstack-pkg-tools.configure-endpoint.templates:6001
msgid ""
"To create the service endpoint, this package needs to know the Admin "
"username, project name, and password, so it can issue commands through the "
"Keystone API."
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:5001
msgid "Keystone admin project name:"
msgstr ""

#. Type: password
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:6001
msgid "Keystone admin password:"
msgstr ""

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:7001
#, fuzzy
#| msgid "Glance endpoint IP address:"
msgid "This service endpoint address:"
msgstr "IP adresa koncového bodu Glance:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:7001
#, fuzzy
#| msgid ""
#| "Enter the IP address that will be used to contact Glance (eg: the Glance "
#| "endpoint IP address)."
msgid ""
"Please enter the endpoint address that will be used to contact this service. "
"You can specify either a Fully Qualified Domain Name (FQDN) or an IP address."
msgstr ""
"Zadejte IP adresu, která se bude používat ke spojení s Glance (např: IP "
"adresa koncového bodu Glance)."

#. Type: select
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:8001
#, fuzzy
#| msgid "Glance endpoint IP address:"
msgid "This service endpoint protocol:"
msgstr "IP adresa koncového bodu Glance:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:9001
msgid "Name of the region to register:"
msgstr "Název registrované oblasti:"

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:9001
#, fuzzy
#| msgid ""
#| "Openstack can be used using availability zones, with each region "
#| "representing a location. Please enter the zone that you wish to use when "
#| "registering the endpoint."
msgid ""
"OpenStack supports using regions, with each region representing a different "
"location (usually a different data center). Please enter the region name "
"that you wish to use when registering the endpoint."
msgstr ""
"Openstack lze využívat pomocí oblastí dostupnosti, přičemž každá oblast "
"představuje místo. Zadejte prosím oblast, kterou chcete použít při "
"registraci koncového bodu."

#. Type: string
#. Description
#: ../openstack-pkg-tools.configure-endpoint.templates:9001
msgid "A non-valid string will abort the API endpoint registration."
msgstr ""

#, fuzzy
#~| msgid "Auth server tenant name:"
#~ msgid "Auth server admin endpoint URL:"
#~ msgstr "Název nájemce pro autentizační server:"
